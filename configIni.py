import configparser
import os

ini = configparser.ConfigParser()

config_file = os.path.join(os.path.dirname(__file__), 'config.ini')

ini.read(config_file)

#path = 'D:\reportingperupurecloud'
#os.chdir(path)

## Load Script Config
def load_config():
    return ini
