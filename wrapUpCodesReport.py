import base64
import requests
import sys
import json
import itertools
import datetime
import configIni

# PureCloud class to encapsulate all high-level logic to access PureCloud API


class WrapUpCodesReport:
    
    def __init__(self, clientId, clientSecret, loginURL, apiURL):
        self.loginURL = loginURL
        self.apiURL = apiURL

        # Base64 encode the client ID and client secret
        self.authorization = base64.b64encode(
            (clientId + ':' + clientSecret).encode()).decode('ascii')

        # Initialize PureCloud access (Token)
        self.token = self.__getToken()

        self.headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json'
        }


   
    # --- Private Interface --------------------------------------------------

    # Getting access to the API token using clientID & secretID

    def __getToken(self):
        # Prepare for POST /oauth/token request
        requestHeaders = {
            'Authorization': 'Basic ' + self.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        requestBody = {
            'grant_type': 'client_credentials'
        }

        # Get token
        response = requests.post(
            self.loginURL + '/oauth/token', data=requestBody, headers=requestHeaders)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            raise Exception('Failure getting token: ' +
                            str(response.status_code) + ' - ' + response.reason)

    # Function to encapsulate WebServices Queries to PureCloud
    # It is a Generator function that returns a pool of arrays with every page fecthed

    def __queryPureCloud(self, config):

        url = self.apiURL + config["url"]

        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        # Loop to query pages
        while True:

            # Pass the params in URL or in the body depending on method
            if (config["method"] == "GET") or (config["method"] == "DELETE"):
                r = apicall(
                    url, params=config["payload"], headers=self.headers)
            else:
                r = apicall(url, json=config["payload"], headers=self.headers)

            try:
                # if no pagination, return data first and finish the function execution (for itineration)

                if config["pagToken"] == None:

                    # We need to return an Array even when there is one element
                    yield [r.json()]
                    break

                # If pagination, return an array of data based on Genertors until the query is finished
                # Eliminated 'pagToken' form JSON object and return raw array

                yield [data for data in r.json()[config["pagToken"]]]

                if r.json() == {}:
                    break
                else:
                    config["payload"]["paging"]["pageNumber"] += 1
            except:
                print("ERROR:" + str(r.status_code))
                break
    # --- Public Interface ----------------------------------------------------

    # pureCloudQuery

    def pureCloudQuery(self, config):

        # TODO: Si hay que hacer paginación, éste es un buen sitio para hacerlo
        # Puede ocurrir que en entornos con mucho tráfico el número de páginas sea muy grande y no se deba
        # guardar en memoria tanta información.

        # Iterator until all pages are fetched & Flatten the list of list returned by __queryPureCloud because every page fetch is an array

        return list(itertools.chain.from_iterable([x for x in self.__queryPureCloud(config)]))

    def purewrapupcodes(self, config):
        now = datetime.datetime.now()
        dt_string1 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha inicio wrapUpCodes =", dt_string1)	
        
        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        rs = []
        wrapupNum = 0
        pageNumber = 1
        while True:
            r = apicall(url, params=config["payload"], headers=self.headers)
            if pageNumber <= r.json()["pageCount"]:
                for entity in r.json()["entities"]:
                # results[entity["id"]] = entity["id"]
                    idWrapup = entity["id"] 
                    nameWrapup = entity["name"]
                    result = {
                            "id": idWrapup,
                            "name": nameWrapup
                    }
                    wrapupNum += 1
                    rs.append(result)
                pageNumber += 1
                config["payload"]["pageNumber"] += 1
            else:
                break
        
        now = datetime.datetime.now()
        dt_string2 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha Fin wrapUpCodes =", dt_string2)
     
        fecha1 = datetime.datetime.strptime(dt_string1, '%d/%m/%Y %H:%M:%S')
        fecha2 = datetime.datetime.strptime(dt_string2, '%d/%m/%Y %H:%M:%S')
        dias = (fecha2 - fecha1) 
        print("Tiempo de proceso: ",round(dias.seconds/60,2), "minutos") 
        print("N. wrapUpCodes: ",wrapupNum)
        return(rs)