#!/bin/bash

declare -a array
#python script.py
#rm -rf /output/data/*csv
rm -rf /home/marlonleon/CONTACT CENTER/REPORTERIA/NUEVA REPORTERIA/reportingperupurecloud/procesado.csv

a=0
for i in "$@"
do
    array[$a]=$i
    a=$((a+1))
done

# get length of an array
arraylength=${#array[@]}

#echo "size :  ${arraylength} "

if [ ${arraylength} -gt 0 ]
then
    echo "Download appi data Purecloud 1..."
    #/opt/rh/rh-python36/root/bin/python3.6 script.py
    #python3 script.py
#    nombre=${array[0]}_${array[1]}
    nombre=${array[0]}}
    python3 main.py ${array[0]} 
    ret_value=$?
else
    echo "Download appi data Purecloud 2..."
    #/opt/rh/rh-python36/root/bin/python3.6 script.py
    python3 script.py
    ret_value=$?
    nombre=$(python3 -c "import utilidades; utilidades.obtener_intervalo()")
    nombre="${nombre//\//_}"
fi

rm -rf __pycache__
echo "Fin..."
