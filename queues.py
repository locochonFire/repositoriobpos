import configparser
import os

ini = configparser.ConfigParser()

config_file = os.path.join(os.path.dirname(__file__), 'queues.ini')

ini.read(config_file)


## Load Script Config
def load_config():
    return ini
