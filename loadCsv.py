import csv

csv.field_size_limit(100000000)

dates = []
data = []
cabecera = []

# Funcion para obtener colas unicas
def unique(list1): 
  
    # intilize a null list 
    unique_list = [] 
      
    # traverse for all elements 
    for x in list1: 
        # check if exists in unique_list or not 
        if x not in unique_list: 
            unique_list.append(x) 
    # print list 
    return unique_list
# fin
  


# Leo CSV
rownum = 0

with open('conversationsDetails2020-01-07_00_23.csv') as csvDataFile:
    csvReader = csv.reader(csvDataFile, delimiter=';')
    for row in csvReader:
        if rownum == 0:
            cabecera = row
        else:
            data.append(
                {
                    str(cabecera[0]): row[0],str(cabecera[1]): row[1],str(cabecera[2]): row[2],
                    str(cabecera[3]): row[3],str(cabecera[4]): row[4],str(cabecera[5]): row[5],
                    str(cabecera[6]): row[6],str(cabecera[7]): row[7],str(cabecera[8]): row[8],
                    str(cabecera[9]): row[9],str(cabecera[10]): row[10],str(cabecera[11]): row[11],
                    str(cabecera[12]): row[12],str(cabecera[13]): row[13],str(cabecera[14]): row[14],
                    str(cabecera[15]): row[15],str(cabecera[16]): row[16],str(cabecera[17]): row[17],
                    str(cabecera[18]): row[18],str(cabecera[19]): row[19],str(cabecera[20]): row[20],
                    str(cabecera[21]): row[21],str(cabecera[22]): row[22],str(cabecera[23]): row[23],
                    str(cabecera[24]): row[24],str(cabecera[25]): row[25],str(cabecera[26]): row[26],
                    str(cabecera[27]): row[27],str(cabecera[28]): row[28],str(cabecera[29]): row[29],
                    str(cabecera[30]): row[30],str(cabecera[31]): row[31],str(cabecera[32]): row[32],
                    str(cabecera[33]): row[33],str(cabecera[34]): row[34],str(cabecera[35]): row[35],
                    str(cabecera[36]): row[36],str(cabecera[37]): row[37],str(cabecera[38]): row[38],
                    str(cabecera[39]): row[39],str(cabecera[40]): row[40],str(cabecera[41]): row[41],
                    str(cabecera[42]): row[42],str(cabecera[43]): row[43],str(cabecera[44]): row[44],
                    str(cabecera[45]): row[45],str(cabecera[46]): row[46],str(cabecera[47]): row[47],
                    str(cabecera[48]): row[48]
                }
            )

        rownum += 1

#Obtengo colas unicas
queueList = []
for queue in data :
    queueList.append(queue.get('nameAcd'))

queueList = unique(queueList)

#queueList = ['44842ef3-e11f-4811-bc41-d2d34b9babdf']
#print(queueList)

for nameAcd in  queueList :
    filtered_names = list(filter(lambda item: str(nameAcd) in item.get('nameAcd') , data))
    print(filtered_names.__len__())

# Fin

#print(dates[1].get('nameAcd'))
#print(cabecera.__len__())
#print(data)
