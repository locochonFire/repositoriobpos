import sys
import json
import csv
from conversationDetailReport import ConversationDetailReport
from queueReport import QueueReport
from wrapUpCodesReport import WrapUpCodesReport
import time
import datetime
import configIni

# Getting PureCloud Token
pc = ConversationDetailReport("8 ", "yheg",
                              "https://login.mypurecloud.com", "https://api.mypurecloud.com")
wc = WrapUpCodesReport("8858 d8", "yhY_k9j3w eg",
                       "https://login.mypurecloud.com", "https://api.mypurecloud.com")


# Returning conversations information
def conversationsDetailsQuery(interval, offset):
    config = {
        "url": "/api/v2/analytics/conversations/details/query",
        "pagToken": None,  # None for no pagination
        "method": "POST",  # GET/POST/DELETE/PUT/PATCH
        "offset": offset,
        "dateFormat": "%Y-%m-%dT%H:%M:%S",
        "payload": {
            "interval": interval,
            "order": "asc",
            "paging": {
                "pageSize": 100,
                "pageNumber": 1
            }
        }
    }

    results = pc.pureCloudConversations(config)
    return results


def wrapupcodes() -> object:
    config = {
        "url": "/api/v2/routing/wrapupcodes",
        "pagToken": None,  # None for no pagination
        "method": "GET",  # GET/POST/DELETE/PUT/PATCH
        "payload": {
            "pageSize": "100",
            "sortBy": "name",
            "pageNumber": 1
        }
    }

    results = wc.purewrapupcodes(config)

    return results


if __name__ == "__main__":
    print('''           
     ###                                   i###    
    L   #                                 .        
   #                                     #        
 ###   .                                 #    ##  
#      #                                  #     j 
#      W#                                #        
#       D#                              #       i 
 #  #i   f#                            #    ## ;# 
     #t   ,#                          #    #      
      #t   .#                        #    #       
       #j    W                      #    #        
        Wj   tW      D######.      #    #         
         Wj   ,#   ##,,,,,,,L##   #    #          
           #    ##i,,,,,,,,,,,,###    #           
            #  ##,,,,,,,,,,,,,,,,#,  #            
             L##,,,,,,,,,,,,,,,,,,#:#             
             K#,,,,,,,,,,,,,,,,,,,,#              
             #,,,,,,,,,,:,,,,,,,,,,;#             
            Wt#######WKEEEKW#######K#             
            #LLLLLLLLLLLLLLLLLLLLLLLGK            
           ;E#######WDLjtjLK##########            
        K##i,,,,,,,,,,,,,,,,,,,,,,,,,:E##;        
     :#t,,,,,,W######################,,,,,;#K     
    #,,,,,,Wj                        i#,,,,,,#:   
    W##,,,,#                          #,,,,G#;    
        .###     ####        ###j     ###K        
           #   :######      ######    #           
           #   #######G    #######K  i#           
           #  :########    ########  #G           
           j# L########    ########  #            
            #  ########    ########  #            
            #. #######     #######  #             
             #  #####j      #####t .#             
             ##   KL    ##    t:   ##             
            i ##       ###t       #  #            
           i:  K#      f##       #    #           
          i,   ,##             ## #    #          
         #    #   ;##       f##    #    #         
        #    #     E #######K K     #    #        
       #    D          t    ;i#      #    #       
      #    W:     ..D########         #    #      
     #    #,      W    ;  #            #    # ##. 
 #  #    #:       ###     #  ##         #    .  j 
j       #,        #  ,#######  #         #       #
#      #,         t:          #           #      #
E      #           #E        #E           #     K 
 .##                 #t    E#                 #t  
   #                  D####f                  #   
   #   #                                  #       
    ###                                    ,#:  
                   CONTACT CENTER   

            ''')

    lista = []
    if (len(sys.argv) > 1):
        for arg in sys.argv:
            if (arg != ''):
                lista.append(arg)

    offset = -5
    intervalStart = "00:00"
    intervalEnd = "23:59"
    tipoReporte = input("Introduce el tipo de reporte 'd' para reporte diario o 'r' para reporte por rango de fechas:")
    if tipoReporte:
        if tipoReporte == "d":
            dateInterval = input("Introduce la fecha en formato YYYY-MM-DD: ")
            if len(dateInterval) != 10:
                sys.exit("El formato de fecha no es correcto.")

            try:
                intervalStart2 = datetime.datetime.strptime(
                    dateInterval + "T" + intervalStart + ":00", "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(hours=offset)
            except:
                sys.exit("El formato de hora no es correcto.")

            try:
                intervalEnd2 = datetime.datetime.strptime(
                    dateInterval + "T" + intervalEnd + ":59", "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(hours=offset)
            except:
                sys.exit("El formato de hora no es correcto.")
        elif tipoReporte == "r":
            dateIntervalStart = input("Introduce la fecha inicio  en formato YYYY-MM-DD: ")
            dateIntervalEnd = input("Introduce la fecha Fin  en formato YYYY-MM-DD: ")
            if len(dateIntervalStart) != 10:
                sys.exit("El formato de fecha inicio no es correcto.")
            try:
                intervalStart2 = datetime.datetime.strptime(
                    dateIntervalStart + "T" + intervalStart + ":00", "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(
                    hours=offset)
            except:
                sys.exit("El formato de hora no es correcto.")

            if len(dateIntervalEnd) != 10:
                sys.exit("El formato de fecha fin no es correcto.")
            try:
                intervalEnd2 = datetime.datetime.strptime(
                    dateIntervalEnd + "T" + intervalEnd + ":59", "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(hours=offset)
            except:
                sys.exit("El formato de hora no es correcto.")

            if intervalStart2 > intervalEnd2:
                sys.exit("La fecha de inicio no puede ser mayor a la fecha fin.")
        else:
            sys.exit("Ingrese 'd' para reporte diario o 'r' para reporte por rango de fechas")
    else:
        sys.exit("Ingrese 'd' para reporte diario o 'r' para reporte por rango de fechas")

    intervalStart2 = (str(intervalStart2) + ".000Z").replace(" ", "T")
    intervalEnd2 = (str(intervalEnd2) + ".000Z").replace(" ", "T")
    intervalFile = intervalStart2[0:10] + "-" + intervalEnd2[0:10]
    interval = intervalStart2 + "/" + intervalEnd2
    print("Reportes generados con el siguiente intervalo:", interval)

    ini = configIni.load_config()
    fileName = ("D:\\reportes\\out\\"+"conversationsDetails" + intervalFile + ".csv")
    print("----> 1 - Inicio de generaciòn fichero {}...".format(fileName),"<----")
    conversations = conversationsDetailsQuery(interval, offset)
    fieldnames = ["idConversation",
                  "idInteraction",
                  "participant",
                  "conversationStart",
                  "conversationEnd",
                  "conversationTime",
                  "originatingDirection",
                  "remoteExternal",
                  "indCallPurecloud",
                  "nameIvr",
                  "segmentStartIvr",
                  "segmentEndIvr",
                  "segmentTimeIvr",
                  "surveyStartIvr",
                  "surveyEndIvr",
                  "surveyTimeIvr",
                  "indSurvey",
                  "authenticationStartIvr",
                  "authenticationEndIvr",
                  "authenticationTimeIvr",
                  "indAuthenticationIvr",
                  "segmentStartAcd",
                  "segmentEndAcd",
                  "segmentTimeAcd",
                  "nameAcd",
                  "mediaTypeAcd",
                  "indAbandonAcd",
                  "abandonTime",
                  "indAbandonTime10",
                  "indAbandonTime15",
                  "indAbandonTime20",
                  "indAbandonTime30",
                  "ani",
                  "dnis",
                  "alertTime",
                  "acwTime",
                  "talkTime",
                  "holdTime",
                  "operationTime",
                  "indOperationTime10",
                  "indOperationTime15",
                  "indOperationTime20",
                  "indOperationTime30",
                  "waitTime",
                  "idWrapup1",
                  "noteWrapup1",
                  "idWrapup2",
                  "noteWrapup2",
                  "idWrapup3",
                  "noteWrapup3",
                  "indDisconnect",
                  "disconnectType",
                  "sessionDnis",
                  "indConsult",
                  "indConsultTransferred",
                  "acdConsult",
                  "indTransfer",
                  "acdTransfer",
                  "intervalTime",
                  "yearConversationStart",
                  "monthConversationStart",
                  "dayConversationStart",
                  "accepted",
                  "attended",
                  "idAgent",
                  "nameAgent",
                  "idManager",
                  "nameManager"]
    with open(fileName, 'w') as outfile:
        csvwriter = csv.DictWriter(
            outfile, delimiter=';', lineterminator='\n', fieldnames=fieldnames)
        csvwriter.writeheader()
        csvwriter.writerows(conversations)
    print("Fin de generaciòn fichero {}...".format(fileName), "<----")
    print(" ")

#Genera Reporte de Colas y Agentes
fileName21 = ("D:\\reportes\\out\\"+"sumQueues" + intervalFile +  ".csv")
fileName22 = ("D:\\reportes\\out\\"+"sumAgents" + intervalFile +  ".csv")
print("----> 2 - Inicio de generaciòn ficheros totalizados:")
print(format(fileName21))
print(format(fileName22))
qr = QueueReport(fileName, fileName21, fileName22)
qr.generateReport()
print("Fin de generaciòn de ficheros totalizados")
print(" ")

#Genera archivo wrapupcodes
fileName3 = ("D:\\reportes\\out\\"+"wrapupcodes" + intervalFile +  ".csv")
print("----> 3 - Inicio de generaciòn fichero {}...".format(fileName3),"<----")
wrapup = wrapupcodes()
fieldnames = ["id", "name"]
with open(fileName3, 'w') as outfile:
    csvwriter = csv.DictWriter(
        outfile, delimiter=';', lineterminator='\n', fieldnames=fieldnames)
    csvwriter.writeheader()
    csvwriter.writerows(wrapup)
print("Fin de generaciòn fichero {}...".format(fileName3), "<----")
print(" ")

print("Generaciòn ok...................!!!!!!!")
input("Presiona una tecla para salir")
time.sleep(2)
sys.exit()
