import base64
import requests
import sys
import json
import itertools
import datetime

# PureCloud class to encapsulate all high-level logic to access PureCloud API


class PureCloud:

    def __init__(self, clientId, clientSecret, loginURL, apiURL):
        self.loginURL = loginURL
        self.apiURL = apiURL

        # Base64 encode the client ID and client secret
        self.authorization = base64.b64encode(
            (clientId + ':' + clientSecret).encode()).decode('ascii')

        # Initialize PureCloud access (Token)
        self.token = self.__getToken()

        self.headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json'
        }

    # --- Private Interface --------------------------------------------------

    # Getting access to the API token using clientID & secretID

    def __getToken(self):
        # Prepare for POST /oauth/token request
        requestHeaders = {
            'Authorization': 'Basic ' + self.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        requestBody = {
            'grant_type': 'client_credentials'
        }

        # Get token
        response = requests.post(
            self.loginURL + '/oauth/token', data=requestBody, headers=requestHeaders)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            raise Exception('Failure getting token: ' +
                            str(response.status_code) + ' - ' + response.reason)

    # Function to encapsulate WebServices Queries to PureCloud
    # It is a Generator function that returns a pool of arrays with every page fecthed

    def __queryPureCloud(self, config):

        url = self.apiURL + config["url"]

        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        # Loop to query pages
        while True:

            # Pass the params in URL or in the body depending on method
            if (config["method"] == "GET") or (config["method"] == "DELETE"):
                r = apicall(
                    url, params=config["payload"], headers=self.headers)
            else:
                r = apicall(url, json=config["payload"], headers=self.headers)

            try:
                # if no pagination, return data first and finish the function execution (for itineration)

                if config["pagToken"] == None:

                    # We need to return an Array even when there is one element
                    yield [r.json()]
                    break

                # If pagination, return an array of data based on Genertors until the query is finished
                # Eliminated 'pagToken' form JSON object and return raw array

                yield [data for data in r.json()[config["pagToken"]]]

                if r.json() == {}:
                    break
                else:
                    config["payload"]["paging"]["pageNumber"] += 1
            except:
                print("ERROR:" + str(r.status_code))
                break
    # --- Public Interface ----------------------------------------------------

    # pureCloudQuery

    def pureCloudQuery(self, config):

        # TODO: Si hay que hacer paginación, éste es un buen sitio para hacerlo
        # Puede ocurrir que en entornos con mucho tráfico el número de páginas sea muy grande y no se deba
        # guardar en memoria tanta información.

        # Iterator until all pages are fetched & Flatten the list of list returned by __queryPureCloud because every page fetch is an array

        return list(itertools.chain.from_iterable([x for x in self.__queryPureCloud(config)]))

    def pureCloudConversations(self, config):

        def calculateDates(fechaStart, fechaEnd, config):
            if fechaStart.find(".") == -1:
                segmentStart = datetime.datetime.strptime(fechaStart.replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
            else:
                segmentStart = datetime.datetime.strptime(fechaStart[:-(len(fechaStart) - fechaStart.find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
            
            if fechaEnd:
                if fechaEnd.find(".") == -1:
                    segmentEnd = datetime.datetime.strptime(fechaEnd.replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                else:
                    segmentEnd = datetime.datetime.strptime(fechaEnd[:-(len(fechaEnd) - fechaEnd.find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
            else:
                segmentEnd = ''
            segmentTime = (segmentEnd - segmentStart).seconds if segmentEnd else '' 
            return segmentStart, segmentEnd , segmentTime


        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        now = datetime.datetime.now()
        print("now =", now)
        dt_string1 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("@@@Fecha inicio Proceso =", dt_string1)	

        results = []
        interactions = 0
        # Loop to query pages
        while True:
            r = apicall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and r.json() != {}:
                interactions += len(r.json()["conversations"])
                print("Generando conversaciones " + str(interactions) + " pagina " +
                      str(config["payload"]["paging"]["pageNumber"])+" Url: "+url)
              
                for c in r.json()["conversations"]:
                    segment = 1     
                      
                    if c["conversationId"] == "7836eaa9-0480-4beb-b6cf-319387c3886c":
        #Campos de la entidad "conversation"                   
                        conversationId= c["conversationId"]
                        interaccion = 1
                        indDirection = c["originatingDirection"]
                        if c["conversationStart"].find(".") == -1:
                            conversationStart = datetime.datetime.strptime(c["conversationStart"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                        else:
                            conversationStart = datetime.datetime.strptime(c["conversationStart"][:-(len(c["conversationStart"]) - c["conversationStart"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                       
                        if "conversationEnd" in c:
                            if c["conversationEnd"].find(".") == -1:
                                conversationEnd = datetime.datetime.strptime(c["conversationEnd"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                            else:
                                conversationEnd = datetime.datetime.strptime(c["conversationEnd"][:-(len(c["conversationEnd"]) - c["conversationEnd"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                        else:                        
                            conversationEnd =  None
                      
                        conversationTime = (conversationEnd - conversationStart).seconds if conversationEnd else None

                        print("uno:x ",conversationStart)
                        print("uno: ",conversationEnd)
                        print("uno: ",conversationTime)

                        calculateDates_segmentStart, calculateDates_segmentEnd , calculateDates_segmentTime = calculateDates(c["conversationStart"], c["conversationEnd"], config)
                        print("dos: ",calculateDates_segmentStart)
                        print("dos: ",calculateDates_segmentEnd)
                        print("dos: ",calculateDates_segmentTime)

                        conversationEnded = ''
                        calculateDates_segmentStart, calculateDates_segmentEnd , calculateDates_segmentTime = calculateDates(c["conversationStart"], conversationEnded , config)
                        print("eee: ",calculateDates_segmentStart)
                        print("eee: ",calculateDates_segmentEnd)
                        print("eee: ",calculateDates_segmentTime)
        #Campos de "participants"
                        for p in c["participants"]:

                            if p["purpose"] == "customer":
                                participantPurpose = p["purpose"] 
                                for s in p["sessions"]:
                                    phoneCustomer  = s["ani"]

                            elif p["purpose"] == "ivr":
                                participantPurpose = p["purpose"]
                                
                                for s in p["sessions"]:
                                    i = 0
                                    for x in s["segments"]:
                                        if i == 0:
                                            if x["segmentStart"].find(".") == -1:
                                                segmentStartIvr = datetime.datetime.strptime(x["segmentStart"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                            else:
                                                segmentStartIvr = datetime.datetime.strptime(x["segmentStart"][:-(len(x["segmentStart"]) - x["segmentStart"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                        if x["segmentEnd"].find(".") == -1:
                                            segmentEndIvr = datetime.datetime.strptime(x["segmentEnd"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        else:
                                            segmentEndIvr = datetime.datetime.strptime(x["segmentEnd"][:-(len(x["segmentEnd"]) - x["segmentEnd"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        i += 1 
                                    segmentTimeIvr: (segmentEndIvr - segmentStartIvr).seconds
                                
                                    if s["flow"]:
                                        f = s["flow"]
                                        nameIvr= f["flowName"]

                            elif p["purpose"] == "acd":
                                participantPurpose = p["purpose"]
                                nameAcd= p["participantName"]

                                for s in p["sessions"]:
                                    mediaTypeAcd = s["mediaType"]
                                    i = 0
                                    for x in s["segments"]:
                                        if i == 0:
                                            if x["segmentStart"].find(".") == -1:
                                                segmentStartAcd = datetime.datetime.strptime(x["segmentStart"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                            else:
                                                segmentStartAcd = datetime.datetime.strptime(x["segmentStart"][:-(len(x["segmentStart"]) - x["segmentStart"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                        if x["segmentEnd"].find(".") == -1:
                                            segmentEndAcd = datetime.datetime.strptime(x["segmentEnd"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        else:
                                            segmentEndAcd = datetime.datetime.strptime(x["segmentEnd"][:-(len(x["segmentEnd"]) - x["segmentEnd"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        i += 1
                                    segmentTimeAcd: (segmentEndAcd - segmentStartAcd).seconds
                                    for m in s["metrics"]:
                                        if m["name"] == "tAbandon":
                                            indAbandonAcd = "1"
                                        else:
                                            indAbandonAcd = "0"

                            elif p["purpose"] == "agent":
                                participantPurpose = p["purpose"]
                                for s in p["sessions"]:
                                    mediaTypeAcd = s["mediaType"]
                                    i = 0
                                    for x in s["segments"]:
                                        
                                        if i == 0:
                                            if x["segmentStart"].find(".") == -1:
                                                segmentStartAcd = datetime.datetime.strptime(x["segmentStart"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                            else:
                                                segmentStartAcd = datetime.datetime.strptime(x["segmentStart"][:-(len(x["segmentStart"]) - x["segmentStart"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                        if x["segmentEnd"].find(".") == -1:
                                            segmentEndAcd = datetime.datetime.strptime(x["segmentEnd"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        else:
                                            segmentEndAcd = datetime.datetime.strptime(x["segmentEnd"][:-(len(x["segmentEnd"]) - x["segmentEnd"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        i += 1
                                    segmentTimeAcd: (segmentEndAcd - segmentStartAcd).seconds
                                    for m in s["metrics"]:
                                        if m["name"] == "tAbandon":
                                            indAbandonAcd = "1"
                                        else:
                                            indAbandonAcd = "0"

                            
   


                            for s in p["sessions"]:
                                for x in s["segments"]:
                                    if x["segmentType"] in ["interact", "alert", "ivr",  "wrapup"]:  
                                        result = {
                                            "conversationId": conversationId,
                                            "participantPurpose": participantPurpose,
                                            "interaccion": interaccion,
                                            "conversationStart (GMT" + str(config["offset"]) + ")": conversationStart,
                                            "conversationEnd (GMT" + str(config["offset"]) + ")": conversationEnd,
                                            "conversationTime": (conversationEnd - conversationStart).seconds if conversationEnd else None ,
                                            "indDirection": indDirection,
                                            "originatingDirection": c["originatingDirection"] if "originatingDirection" in c else None,
                                            "segment": segment,
                                            
                                            "participantPurpose": p["purpose"] if "purpose" in p else None,
                                            "userId": p["userId"] if "userId" in p else None,
                                            "mediaType": s["mediaType"] if "mediaType" in s else None,
                                            "ani": s["ani"][4:] if "ani" in s else None,
                                            "dnis": s["dnis"][4:] if "dnis" in s else None,
                                            "sessionDnis": s["sessionDnis"] if "sessionDnis" in s else None,
                                            "queueId": x["queueId"] if "queueId" in x else None,
                                            "segmentType": x["segmentType"] if "segmentType" in x else None,
                                            "disconnectType": x["disconnectType"] if "disconnectType" in x else None
                                        }
                                        
                                        segment += 1
                                        results.append(result)
                                        break
                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        now = datetime.datetime.now()
        print("now =", now)
        dt_string2 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("@@@Fecha Fin Proceso =", dt_string2)
     
        fecha1 = datetime.datetime.strptime(dt_string1, '%d/%m/%Y %H:%M:%S')
        fecha2 = datetime.datetime.strptime(dt_string2, '%d/%m/%Y %H:%M:%S')
        dias = (fecha2 - fecha1) 
        print("@@@@@ tiempo de proceso: ",dias.seconds) 
        return(results)

    def pureCloudMOS(self, config):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")


        
      
        results = []
        interactions = 0
        # Loop to query pages
        while True:
            r = apicall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and r.json() != {}:
                interactions += len(r.json()["conversations"])
                print("Generando conversaciones " + str(interactions) + " pagina " +
                      str(config["payload"]["paging"]["pageNumber"]))
                for c in r.json()["conversations"]:
                    if c["conversationStart"].find(".") == -1:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"][:-(len(c["conversationStart"]) - c["conversationStart"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                    if c["conversationEnd"].find(".") == -1:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"][:-(len(c["conversationEnd"]) - c["conversationEnd"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    result = {
                        "conversationId": c["conversationId"],
                        "conversationStart (GMT" + str(config["offset"]) + ")": conversationStart,
                        "conversationEnd (GMT" + str(config["offset"]) + ")": conversationEnd,
                        "duration": (conversationEnd - conversationStart).seconds,
                        "originatingDirection": c["originatingDirection"] if "originatingDirection" in c else None,
                        "mediaStatsMinConversationMos": str(c["mediaStatsMinConversationMos"])[0:4] if "mediaStatsMinConversationMos" in c else None,
                        "participants": len(c["participants"]) if "participants" in c else None,
                        "mediaType": c["participants"][0]["sessions"][0]["mediaType"]
                    }
                    results.append(result)
                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        return(results)

    def pureCloudError(self, config):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        results = []
        interactions = 0
        # Loop to query pages
        while True:
            r = apicall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and r.json() != {}:
                interactions += len(r.json()["conversations"])
                print("Generando conversaciones " + str(interactions) + " pagina " +
                      str(config["payload"]["paging"]["pageNumber"]))
                for c in r.json()["conversations"]:
                    segment = 1
                    if c["conversationStart"].find(".") == -1:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"][:-(len(c["conversationStart"]) - c["conversationStart"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                    if c["conversationEnd"].find(".") == -1:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"][:-(len(c["conversationEnd"]) - c["conversationEnd"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    for p in c["participants"]:
                        for s in p["sessions"]:
                            for x in s["segments"]:
                                if x["segmentType"] in ["interact", "alert", "wrapup", "ivr"] and "errorCode" in x:
                                    if x["segmentStart"].find(".") == -1:
                                        segmentStart = datetime.datetime.strptime(x["segmentStart"].replace(
                                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                    else:
                                        segmentStart = datetime.datetime.strptime(x["segmentStart"][:-(len(x["segmentStart"]) - x["segmentStart"].find(
                                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                    if x["segmentEnd"].find(".") == -1:
                                        segmentEnd = datetime.datetime.strptime(x["segmentEnd"].replace(
                                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                    else:
                                        segmentEnd = datetime.datetime.strptime(x["segmentEnd"][:-(len(x["segmentEnd"]) - x["segmentEnd"].find(
                                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                    result = {
                                        "conversationId": c["conversationId"],
                                        "conversationStart (GMT" + str(config["offset"]) + ")": conversationStart,
                                        "conversationEnd (GMT" + str(config["offset"]) + ")": conversationEnd,
                                        "duration": (conversationEnd - conversationStart).seconds,
                                        "originatingDirection": c["originatingDirection"] if "originatingDirection" in c else None,
                                        "segment": segment,
                                        "participantId": p["participantId"] if "participantId" in p else None,
                                        "participantPurpose": p["purpose"] if "purpose" in p else None,
                                        "userId": p["userId"] if "userId" in p else None,
                                        "mediaType": s["mediaType"] if "mediaType" in s else None,
                                        "ani": s["ani"][4:] if "ani" in s else None,
                                        "dnis": s["dnis"][4:] if "dnis" in s else None,
                                        "sessionDnis": s["sessionDnis"] if "sessionDnis" in s else None,
                                        "segmentStart (GMT" + str(config["offset"]) + ")": segmentStart,
                                        "segmentEnd (GMT" + str(config["offset"]) + ")": segmentEnd,
                                        "segmentDuration": (segmentEnd - segmentStart).seconds,
                                        "queueId": x["queueId"] if "queueId" in x else None,
                                        "segmentType": x["segmentType"] if "segmentType" in x else None,
                                        "disconnectType": x["disconnectType"] if "disconnectType" in x else None,
                                        "errorCode": x["errorCode"] if "errorCode" in x else None
                                    }
                                    segment += 1
                                    results.append(result)
                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        return(results)

    def pureCloudConversationOutcomes(self, config, catalog):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        #outcomeSet = set()
        results = []
        interactions = 0
        # Loop to query pages
        while True:
            r = apicall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and r.json() != {}:
                interactions += len(r.json()["conversations"])
                print("Generando conversaciones " + str(interactions) + " pagina " +
                      str(config["payload"]["paging"]["pageNumber"]))
                for c in r.json()["conversations"]:
                    if c["conversationStart"].find(".") == -1:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationStart = datetime.datetime.strptime(c["conversationStart"][:-(len(c["conversationStart"]) - c["conversationStart"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                    if c["conversationEnd"].find(".") == -1:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"].replace(
                            "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    else:
                        conversationEnd = datetime.datetime.strptime(c["conversationEnd"][:-(len(c["conversationEnd"]) - c["conversationEnd"].find(
                            "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                    for p in c["participants"]:
                            for s in p["sessions"]:
                                for x in s["segments"]:
                                    if x["segmentType"] == "ivr":

                                        if x["segmentStart"].find(".") == -1:
                                            segmentStart = datetime.datetime.strptime(x["segmentStart"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        else:
                                            segmentStart = datetime.datetime.strptime(x["segmentStart"][:-(len(x["segmentStart"]) - x["segmentStart"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                        if x["segmentEnd"].find(".") == -1:
                                            segmentEnd = datetime.datetime.strptime(x["segmentEnd"].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                                        else:
                                            segmentEnd = datetime.datetime.strptime(x["segmentEnd"][:-(len(x["segmentEnd"]) - x["segmentEnd"].find("."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])

                                        result = {
                                            "conversationId": c["conversationId"],
                                            "conversationStart (GMT" + str(config["offset"]) + ")": conversationStart,
                                            "conversationEnd (GMT" + str(config["offset"]) + ")": conversationEnd,
                                            "duration": (conversationEnd - conversationStart).seconds,
                                            "segmentType": x["segmentType"] if "segmentType" in x else None,
                                            "segmentStart (GMT" + str(config["offset"]) + ")": segmentStart,
                                            "segmentEnd (GMT" + str(config["offset"]) + ")": segmentEnd,
                                            "segmentDuration": (segmentEnd - segmentStart).seconds,
                                        }
                                        if "flow" in s:
                                            result["flowName"] = s["flow"]["flowName"] if "flowName" in s["flow"] else "sinFlow"
                                            if "outcomes" in s["flow"]:
                                                for o in s["flow"]["outcomes"]:
                                                    flowOutcome = catalog.get(
                                                        o["flowOutcomeId"])
                                                    result[flowOutcome] = o["flowOutcomeValue"] if "flowOutcomeValue" in o else None

                                        results.append(result)
                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        return(results)

    def pureCloudCatalog(self, config):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        pageNumber = 1
        results = {}
        while True:
            r = apicall(url, params=config["payload"], headers=self.headers)
            print('Got page {0} of {1}'.format(
                pageNumber, r.json()["pageCount"]))

            for entity in r.json()["entities"]:
                results[entity["id"]] = entity["name"]

            if pageNumber >= r.json()["pageCount"]:
                print('found:', len(results))
                break

        return(results)

    def pureCloudConversationList(self, config):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        #outcomeSet = set()
        results = []
        interactions = 0
        # Loop to query pages
        while True:
            r = apicall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and r.json() != {}:
                interactions += len(r.json()["conversations"])
                print("Generando conversaciones " + str(interactions) + " pagina " +
                      str(config["payload"]["paging"]["pageNumber"]))
                for c in r.json()["conversations"]:
                    results.append(c["conversationId"])

                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        return(results)

    def pureCloudConversationAttributes(self, config, conversationList):

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apicall = requests.post
        elif config["method"] == "PATCH":
            apicall = requests.patch
        elif config["method"] == "GET":
            apicall = requests.get
        elif config["method"] == "PUT":
            apicall = requests.put
        elif config["method"] == "DELETE":
            apicall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        #outcomeSet = set()
        results = []
        attributeList = []
        count = 1
        # Loop to query pages
        while True:
            for conv in conversationList:
                r = apicall(url + conv, headers=self.headers)
                if r.status_code == 200 and r.json() != {}:
                    print("Descargando conversacion {}: {} de {}".format(conv, count, str(len(conversationList))))
                    for p in r.json()["participants"]:
                        if p["participantType"] == "External" and p["attributes"] != {}:
                            if r.json()["startTime"].find(".") == -1:
                                conversationStart = datetime.datetime.strptime(r.json()["startTime"].replace(
                                    "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                            else:
                                conversationStart = datetime.datetime.strptime(r.json()["startTime"][:-(len(r.json()["startTime"]) - r.json()["startTime"].find(
                                    "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                        
                            if r.json()["endTime"].find(".") == -1:
                                conversationEnd = datetime.datetime.strptime(r.json()["endTime"].replace(
                                    "Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                            else:
                                conversationEnd = datetime.datetime.strptime(r.json()["endTime"][:-(len(r.json()["endTime"]) - r.json()["endTime"].find(
                                    "."))].replace("Z", ""), config["dateFormat"]) + datetime.timedelta(hours=config["offset"])
                            
                            result = {
                                "conversationId": r.json()["id"],
                                "conversationStart (GMT" + str(config["offset"]) + ")": conversationStart,
                                "conversationEnd (GMT" + str(config["offset"]) + ")": conversationEnd,
                                "duration": (conversationEnd - conversationStart).seconds,
                                "purpose": p["purpose"],
                                "ani": p["ani"][4:],
                                "dnis": p["dnis"][4:],
                            }

                            attributes = p["attributes"].items()
                            for attribute, value in attributes:
                                result[attribute] = value if value != "" else "Atributo vacio!"
                                attributeList.append(attribute)

                            results.append(result)
                
                   
                else:
                    break
                count += 1
            break

        attributeList = list(set(attributeList))
        return(results, attributeList)


