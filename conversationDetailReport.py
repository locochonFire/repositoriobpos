import base64
from datetime import datetime

import requests
import sys
import json
import itertools
import datetime
import queues


# PureCloud class to encapsulate all high-level logic to access PureCloud API


class ConversationDetailReport:

    def __init__(self, clientId, clientSecret, loginURL, apiURL):
        self.loginURL = loginURL
        self.apiURL = apiURL
        self.arreglo_agents = []
        self.arregloidem_agents = []
        self.arreglo_managers = []
        self.arregloidem_managers = []

        # Base64 encode the client ID and client secret
        self.authorization = base64.b64encode(
            (clientId + ':' + clientSecret).encode()).decode('ascii')

        # Initialize PureCloud access (Token)
        self.token = self.__getToken()

        self.headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json'
        }

    # --- Private Interface --------------------------------------------------

    # Getting access to the API token using clientID & secretID

    def __getToken(self):
        # Prepare for POST /oauth/token request
        requestHeaders = {
            'Authorization': 'Basic ' + self.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        requestBody = {
            'grant_type': 'client_credentials'
        }

        # Get token
        response = requests.post(
            self.loginURL + '/oauth/token', data=requestBody, headers=requestHeaders)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            raise Exception('Failure getting token: ' +
                            str(response.status_code) + ' - ' + response.reason)

    def pureagent(self, userId):

        config = {
            "url": "/api/v2/users/" + userId + "",
            "pagToken": None,  # None for no pagination
            "method": "GET",  # GET/POST/DELETE/PUT/PATCH
            "payload": {
            }
        }

        url = self.apiURL + config["url"]
        # Set WebService Method

        if config["method"] == "POST":
            apiCall = requests.post
        elif config["method"] == "PATCH":
            apiCall = requests.patch
        elif config["method"] == "GET":
            apiCall = requests.get
        elif config["method"] == "PUT":
            apiCall = requests.put
        elif config["method"] == "DELETE":
            apiCall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        pageNumber = 1
        result = {}
        r = apiCall(url, params=config["payload"], headers=self.headers)
        # print(r.json())
        if "manager" in r.json():
            result = {
                "id": r.json()["id"],
                "userName": r.json()["username"],
                "name": r.json()["name"],
                "idManager": r.json()["manager"]["id"]
            }
        elif "id" in r.json():
            result = {
                "id": r.json()["id"],
                "userName": r.json()["username"],
                "name": r.json()["name"]
            }
        else:
            result = {
                "id": "",
                "userName": "",
                "name": ""
            }
        return (result)

    def getQueueName(self, queueId):
        # req_header = {
        #     'Authorization': token['token_type'] + ' ' + token['access_token']
        # }
        url = "https://api.mypurecloud.com/api/v2/routing/queues/{}/".format(queueId)
        data = requests.get(url, headers=self.headers)
        queueName = data.json()

        if data.status_code == 404:
            return queueId
        else:
            return queueName['name']

    # Function to encapsulate WebServices Queries to PureCloud
    # It is a Generator function that returns a pool of arrays with every page fecthed

    def __queryPureCloud(self, config):

        url = self.apiURL + config["url"]

        # Set WebService Method

        if config["method"] == "POST":
            apiCall = requests.post
        elif config["method"] == "PATCH":
            apiCall = requests.patch
        elif config["method"] == "GET":
            apiCall = requests.get
        elif config["method"] == "PUT":
            apiCall = requests.put
        elif config["method"] == "DELETE":
            apiCall = requests.delete
        else:
            raise Exception("Method not correctly configured")

        # Loop to query pages
        while True:

            # Pass the params in URL or in the body depending on method
            if (config["method"] == "GET") or (config["method"] == "DELETE"):
                r = apiCall(
                    url, params=config["payload"], headers=self.headers)
            else:
                r = apiCall(url, json=config["payload"], headers=self.headers)

            try:
                # if no pagination, return data first and finish the function execution (for itineration)

                if config["pagToken"] == None:
                    # We need to return an Array even when there is one element
                    yield [r.json()]
                    break

                # If pagination, return an array of data based on Genertors until the query is finished
                # Eliminated 'pagToken' form JSON object and return raw array

                yield [data for data in r.json()[config["pagToken"]]]

                if r.json() == {}:
                    break
                else:
                    config["payload"]["paging"]["pageNumber"] += 1
            except:
                print("ERROR:" + str(r.status_code))
                break

    # --- Public Interface ----------------------------------------------------

    # pureCloudQuery

    def pureCloudQuery(self, config):

        # TODO: Si hay que hacer paginación, éste es un buen sitio para hacerlo
        # Puede ocurrir que en entornos con mucho tráfico el número de páginas sea muy grande y no se deba
        # guardar en memoria tanta información.

        # Iterator until all pages are fetched & Flatten the list of list returned by __queryPureCloud because every page fetch is an array

        return list(itertools.chain.from_iterable([x for x in self.__queryPureCloud(config)]))

    def pureCloudConversations(self, config):
        results = []

        ini = queues.load_config()

        def calculateDates(fechaStart, fechaEnd, config):

            if fechaStart.find(".") == -1:
                segmentStart = datetime.datetime.strptime(fechaStart.replace("Z", ".000"),
                                                          "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                    hours=config["offset"])
            else:
                segmentStart = datetime.datetime.strptime(fechaStart.replace("Z", ""),
                                                          "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                    hours=config["offset"])

            if fechaStart.find(".") == -1:
                segmentStart = datetime.datetime.strptime(fechaStart.replace("Z", ".000"),
                                                          "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                    hours=config["offset"])
            else:
                segmentStart = datetime.datetime.strptime(fechaStart.replace("Z", ""),
                                                          "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                    hours=config["offset"])

            if fechaEnd:
                if fechaEnd.find(".") == -1:
                    segmentEnd = datetime.datetime.strptime(fechaEnd.replace("Z", ".000"),
                                                            "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                        hours=config["offset"])
                else:
                    segmentEnd = datetime.datetime.strptime(fechaEnd.replace("Z", ""),
                                                            "%Y-%m-%dT%H:%M:%S.%f") + datetime.timedelta(
                        hours=config["offset"])

                diff = segmentEnd - segmentStart
                millis = diff.days * 24 * 60 * 60 * 1000
                millis += diff.seconds * 1000
                millis += diff.microseconds / 1000
                segmentTime = millis / 1000
            else:
                segmentTime = 0
                segmentEnd = ""
                # print ("segmentEnd: ",segmentEnd," ","segmentStart: ", segmentStart, " ","segmentTime: ",segmentTime)
            return segmentStart, segmentEnd, segmentTime

        results = []

        def assignInterval(fechaInicio):
            hora = str(fechaInicio.time())[0:5]
            horaInicio = str(hora[0:2])
            minutoInicio = str(hora[3:5])

            if minutoInicio > "14":
                if minutoInicio > "29":
                    if minutoInicio > "44":
                        minIntervalInicio = "45"
                    else:
                        minIntervalInicio = "30"
                else:
                    minIntervalInicio = "15"
            else:
                minIntervalInicio = "00"

            intervalTime = horaInicio + ":" + minIntervalInicio

            return intervalTime

        def addResult(interaccion):
            accepted = 0
            attended = 0
            if participant == ("agent" or "external"):
                accepted = 1
                attended = 1
            elif participant == "acd":
                accepted = 1
                attended = 0

            result = {
                "idConversation": idConversation,
                "idInteraction": idInteraction,
                "participant": participant,
                "conversationStart": conversationStart,
                "conversationEnd": conversationEnd,
                "conversationTime": conversationTime,
                "originatingDirection": indDirection,
                "remoteExternal": remoteExternal,
                "indCallPurecloud": indCallPurecloud,
                "nameIvr": nameIvr,
                "segmentStartIvr": segmentStartIvr,
                "segmentEndIvr": segmentEndIvr,
                "segmentTimeIvr": segmentTimeIvr,
                "surveyStartIvr": surveyStartIvr,
                "surveyEndIvr": surveyEndIvr,
                "surveyTimeIvr": surveyTimeIvr,
                "indSurvey": indSurvey,
                "authenticationStartIvr": authenticationStartIvr,
                "authenticationEndIvr": authenticationEndIvr,
                "authenticationTimeIvr": authenticationTimeIvr,
                "indAuthenticationIvr": indAuthentication,
                "segmentStartAcd": segmentStartAcd,
                "segmentEndAcd": segmentEndAcd,
                "segmentTimeAcd": segmentTimeAcd,
                "nameAcd": nameAcd,
                "mediaTypeAcd": mediaTypeAcd,
                "indAbandonAcd": indAbandonAcd,
                "abandonTime": abandonTime,
                "indAbandonTime10": indAbandonTime10,
                "indAbandonTime15": indAbandonTime15,
                "indAbandonTime20": indAbandonTime20,
                "indAbandonTime30": indAbandonTime30,
                "ani": ani,
                "dnis": dnis,
                "alertTime": alertTime,
                "acwTime": acwTime,
                "talkTime": talkTime,
                "holdTime": holdTime,
                "operationTime": operationTime,
                "indOperationTime10": indOperationTime10,
                "indOperationTime15": indOperationTime15,
                "indOperationTime20": indOperationTime20,
                "indOperationTime30": indOperationTime30,
                "waitTime": waitTime,
                "idWrapup1": idWrapup1,
                "noteWrapup1": noteWrapup1,
                "idWrapup2": idWrapup2,
                "noteWrapup2": noteWrapup2,
                "idWrapup3": idWrapup3,
                "noteWrapup3": noteWrapup3,
                "indDisconnect": indDisconnect,
                "disconnectType": disconnectType,
                "sessionDnis": sessionDnis,
                "indConsult": indConsult,
                "indConsultTransferred": indConsultTransferred,
                "acdConsult": acdConsult,
                "indTransfer": indTransfer,
                "acdTransfer": acdTransfer,
                "intervalTime": assignInterval(conversationStart),
                "yearConversationStart": yearConversationStart,
                "monthConversationStart": monthConversationStart,
                "dayConversationStart": dayConversationStart,
                "accepted": accepted,
                "attended": attended,
                "idAgent": idAgent,
                "nameAgent": "",
                "idManager": "",
                "nameManager": ""
            }
            # se agrega descripcion de las colas
            if result['mediaTypeAcd'] == "callback" or c["originatingDirection"] == "outbound" and notAcd:
                if nameAcd:
                    cola = result['nameAcd']
                    descripcion = self.getQueueName(cola)
                    if descripcion:
                        result['nameAcd'] = descripcion
                    else:
                        result['nameAcd'] = cola

            # Se obtiene el username y nombre del agente y supervisor en base al id del agente
            if participant == "agent":
                iUser = {}
                if result["idAgent"]:
                    if result["idAgent"] not in self.arregloidem_agents:
                        user = self.pureagent(result["idAgent"])
                        iUser["idAgent"] = result["idAgent"]
                        if user:
                            iUser["userNameAgent"] = user["userName"]
                            iUser["nameAgent"] = user["name"]
                            result["idAgent"] = user["userName"]
                            result["nameAgent"] = user["name"]
                        if "idManager" in user:
                            if user["idManager"] not in self.arregloidem_managers:
                                manager = self.pureagent(user["idManager"])
                                if manager:
                                    iUser["idManager"] = user["idManager"]
                                    iUser["userNameManager"] = manager["userName"]
                                    iUser["nameManager"] = manager["name"]
                                    result["idManager"] = manager["userName"]
                                    result["nameManager"] = manager["name"]
                                else:
                                    iUser["idManager"] = user["idManager"]
                                    iUser["userNameManager"] = ""
                                    iUser["nameManager"] = ""
                                    result["idManager"] = user["idManager"]
                                    result["nameManager"] = ""
                                self.arregloidem_managers.append(user["idManager"])
                            else:
                                filtered_manager = list(
                                    filter(lambda item: str(user["idManager"]) in item.get('idManager'),
                                           self.arreglo_agents))
                                iUser["idManager"] = user["idManager"]
                                iUser["userNameManager"] = filtered_manager[0]["userNameManager"]
                                iUser["nameManager"] = filtered_manager[0]["nameManager"]
                                result["idManager"] = filtered_manager[0]["userNameManager"]
                                result["nameManager"] = filtered_manager[0]["nameManager"]
                        else:
                            iUser["idManager"] = ""
                            iUser["userNameManager"] = ""
                            iUser["nameManager"] = ""
                            result["idManager"] = ""
                            result["nameManager"] = ""
                        self.arregloidem_agents.append(user["id"])
                        self.arreglo_agents.append(iUser)
                    else:
                        filtered_agent = list(
                            filter(lambda item: str(result["idAgent"]) in item.get('idAgent'), self.arreglo_agents))
                        result["idAgent"] = filtered_agent[0]["userNameAgent"]
                        result["nameAgent"] = filtered_agent[0]["nameAgent"]
                        if "idManager" in filtered_agent[0]:
                            result["idManager"] = filtered_agent[0]["userNameManager"]
                            result["nameManager"] = filtered_agent[0]["nameManager"]
                        else:
                            result["idmanager"] = ""
                            result["nameManager"] = ""

            results.append(result)

        url = self.apiURL + config["url"]
        # Set WebService Method
        apiCall = requests.post

        now = datetime.datetime.now()
        dt_string1 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha inicio conversationDetails: ", dt_string1)
        fechaEjec: datetime = datetime.datetime.strptime(dt_string1, '%d/%m/%Y %H:%M:%S')
        ejec = 1

        # interactions = 0
        numConversations = 0
        # Loop to query pages
        while True:
            now = datetime.datetime.now()
            dt_string2 = now.strftime("%d/%m/%Y %H:%M:%S")
            fecha2 = datetime.datetime.strptime(dt_string2, '%d/%m/%Y %H:%M:%S')
            restaEjec = (fecha2 - fechaEjec)
            minEjec = round(restaEjec.seconds / 60, 2)
            if minEjec > ejec:
                timeEjec = ""
                for i in range(ejec):
                    timeEjec += "*"
                print(timeEjec)
                ejec += 1
            r = apiCall(url, json=config["payload"], headers=self.headers)
            if r.status_code == 200 and "conversations" in r.json():
                for c in r.json()["conversations"]:
                    # CONVERSATIONS
                    idConversation = ''
                    idInteraction = 0
                    participant = ''
                    participantX = ''
                    conversationStart = ''
                    conversationEnd = ''
                    conversationTime = 0
                    indDirection = 0
                    remoteExternal = ''
                    indCallPurecloud = 0
                    # CUSTOMER
                    ani = ''
                    dnis = ''
                    # IVR
                    nameIvr = ''
                    segmentStartIvr = ''
                    segmentEndIvr = ''
                    segmentTimeIvr = 0
                    surveyStartIvr = ''
                    surveyEndIvr = ''
                    surveyTimeIvr = 0
                    authenticationStartIvr = ''
                    authenticationEndIvr = ''
                    authenticationTimeIvr = 0
                    indAuthentication = 0
                    # ACD
                    segmentStartAcd = ''
                    segmentEndAcd = ''
                    segmentTimeAcd = 0
                    nameAcd = ''
                    mediaTypeAcd = ''
                    indAbandonAcd = 0
                    # if c["conversationId"] == "743338ff-08fb-45f3-aa5d-6b91d9def79c": # or c["conversationId"] =="faa9bf36-776c-488d-8dea-c5ad677522ab" or c["conversationId"] == "1638f416-6f5c-406e-940b-02714d0d9bcc" or c["conversationId"] == "502ab09f-d547-4537-8c7a-a2ccfb4ca55c" or c["conversationId"] ==  "e1b8cf43-a1f9-4cf9-bf7c-300e71c198bf" or c["conversationId"] == "ce2468f2-ed8b-4c73-af2b-e5c17acd77dc" or c["conversationId"] == "8e952261-aac9-4230-a2d7-8491958b5eb8" or c["conversationId"] == "1c00bdd3-78d6-407a-af37-e3d2ed88a6db" or c["conversationId"] ==  "e1b8cf43-a1f9-4cf9-bf7c-300e71c198bf" or c["conversationId"] == "6c894f7c-5973-4f24-a646-4533ad290257" or c["conversationId"] == "990f1e71-a4cf-43e4-b70c-d15d4e7106fb" or c["conversationId"] == "41f33038-47a3-44e8-96e9-248a20e49516":
                    # CONVERSATION
                    idConversation = c["conversationId"]
                    # print("idConversation: ",idConversation)
                    alertTime = 0
                    acwTime = 0
                    talkTime = 0
                    holdTime = 0
                    waitTime = 0
                    abandonTime = 0
                    operationTime = 0
                    indOperationTime10 = 0
                    indOperationTime15 = 0
                    indOperationTime20 = 0
                    indOperationTime30 = 0
                    idWrapup1 = ''
                    noteWrapup1 = ''
                    idWrapup2 = ''
                    noteWrapup2 = ''
                    idWrapup3 = ''
                    noteWrapup3 = ''
                    indDisconnect = 0
                    disconnectType = 0
                    sessionDnis = ''
                    idAgent = ''
                    indTransfer = 0
                    indSurvey = 0
                    indDirection = 0
                    notAcd = 1
                    idInteraction += 1
                    indDirection = c["originatingDirection"]
                    if "conversationEnd" in c:
                        conversationEndX = c["conversationEnd"]
                    else:
                        conversationEndX = ""
                    numConversations += 1
                    calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                        c["conversationStart"], conversationEndX, config)
                    conversationStart = calculateDates_segmentStart
                    conversationEnd = calculateDates_segmentEnd
                    conversationTime = calculateDates_segmentTime
                    yearConversationStart = str(conversationStart.year)
                    monthConversationStart = str(conversationStart.month)
                    dayConversationStart = str(conversationStart.day)

                    # PARTICIPANTS
                    indTransferConsult = 0
                    indTransferConsultTransferred = 0
                    valueTransferConsult = 0
                    valueTransferConsultTransferred = 0
                    indConsultTransferredX = 0
                    indTransferredX = 0
                    indConsultY = 0
                    intervalTime = 0
                    attended = 0
                    accepted = 0
                    for p in c["participants"]:
                        indTransfer = 0
                        acdTransfer = ''
                        indConsult = 0
                        indConsultTransferred = 0
                        acdConsult = ''
                        indAbandonAcd = 0
                        abandonTime = 0
                        indAbandonTime10 = 0
                        indAbandonTime15 = 0
                        indAbandonTime20 = 0
                        indAbandonTime30 = 0
                        # USER
                        if p["purpose"] == "user":
                            participant = p["purpose"]
                            if "ani" in p:
                                ani = s["ani"]
                            if "dnis" in p:
                                dnis = s["dnis"]
                            if "sessions" in p:
                                indCallPurecloud = 1
                                # CUSTOMER
                        elif p["purpose"] == "customer":
                            participant = p["purpose"]
                            for s in p["sessions"]:
                                if "ani" in s:
                                    ani = s["ani"]
                                if "dnis" in s:
                                    dnis = s["dnis"]
                        # IVR
                        elif p["purpose"] == "ivr":
                            participant = p["purpose"]
                            for s in p["sessions"]:
                                i = 0
                                indEndIvr = 0
                                if "flow" in s:
                                    f = s["flow"]
                                    nameIvrX = f["flowName"]
                                    if nameIvrX[0:3] == "IVR" or nameIvrX == "FLUJO DE RECEPCIÓN DE TRANSFERENCIAS":
                                        nameIvr = f["flowName"]
                                        for x in s["segments"]:
                                            if i == 0:
                                                segmentStartIvrFirst = x["segmentStart"]
                                            if "segmentEnd" in x:
                                                segmentEndIvrLast = x["segmentEnd"]
                                            else:
                                                segmentEndIvrLast = ""
                                            i += 1
                                            # Identfica salida del IVR
                                            if mediaTypeAcd != "chat":
                                                if "disconnectType" in x:
                                                    if x["disconnectType"] != "transfer":
                                                        indEndIvr = 1
                                                        indDisconnect = 1
                                                        disconnectType = x["disconnectType"]
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartIvrFirst, segmentEndIvrLast, config)
                                        segmentStartIvr = calculateDates_segmentStart
                                        segmentEndIvr = calculateDates_segmentEnd
                                        segmentTimeIvr = calculateDates_segmentTime
                                        if indEndIvr:
                                            participant = p["purpose"]
                                            addResult(idInteraction)
                                            idInteraction += 1
                                            alertTime = 0
                                            acwTime = 0
                                            talkTime = 0
                                            holdTime = 0
                                            waitTime = 0
                                            operationTime = 0
                                            indOperationTime10 = 0
                                            indOperationTime15 = 0
                                            indOperationTime20 = 0
                                            indOperationTime30 = 0
                                            abandonTime = 0
                                            idAgent = ''
                                            idWrapup1 = ''
                                            noteWrapup1 = ''
                                            idWrapup2 = ''
                                            noteWrapup2 = ''
                                            idWrapup3 = ''
                                            noteWrapup3 = ''
                                            indDisconnect = 0
                                            disconnectType = 0
                                    elif nameIvrX[0:3] == "ENC":
                                        for x in s["segments"]:
                                            if i == 0:
                                                segmentStartIvrFirst = x["segmentStart"]
                                            if "segmentEnd" in x:
                                                segmentEndIvrLast = x["segmentEnd"]
                                            else:
                                                segmentEndIvrLast = ""
                                            i += 1
                                            if "disconnectType" in x:
                                                if x["disconnectType"] != "transfer":
                                                    indEndIvr = 1
                                                    indDisconnect = 1
                                                    disconnectType = x["disconnectType"]
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartIvrFirst, segmentEndIvrLast, config)
                                        surveyStartIvr = calculateDates_segmentStart
                                        surveyEndIvr = calculateDates_segmentEnd
                                        surveyTimeIvr = calculateDates_segmentTime
                                        indSurvey = 1
                                        if participantX:
                                            participant = participantX
                                        else:
                                            participant = p["purpose"]
                                        addResult(idInteraction)
                                        alertTime = 0
                                        acwTime = 0
                                        talkTime = 0
                                        holdTime = 0
                                        waitTime = 0
                                        operationTime = 0
                                        indOperationTime10 = 0
                                        indOperationTime15 = 0
                                        indOperationTime20 = 0
                                        indOperationTime30 = 0
                                        abandonTime = 0
                                        idAgent = ''
                                        idWrapup1 = ''
                                        noteWrapup1 = ''
                                        idWrapup2 = ''
                                        noteWrapup2 = ''
                                        idWrapup3 = ''
                                        noteWrapup3 = ''
                                        indDisconnect = 0
                                        disconnectType = 0
                                    elif nameIvrX[0:3] == "ISL":
                                        for x in s["segments"]:
                                            if i == 0:
                                                segmentStartIvrFirst = x["segmentStart"]
                                            if "segmentEnd" in x:
                                                segmentEndIvrLast = x["segmentEnd"]
                                            else:
                                                segmentEndIvrLast = ""
                                            i += 1
                                            if "disconnectType" in x:
                                                if x["disconnectType"] != "transfer":
                                                    indEndIvr = 1
                                                    indDisconnect = 1
                                                    disconnectType = x["disconnectType"]
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartIvrFirst, segmentEndIvrLast, config)
                                        authenticationStartIvr = calculateDates_segmentStart
                                        authenticationEndIvr = calculateDates_segmentEnd
                                        authenticationTimeIvr = calculateDates_segmentTime
                                        indAuthentication = 1
                                        if indEndIvr:

                                            participant = p["purpose"]
                                            if participantX:
                                                participant = participantX
                                            else:
                                                participant = p["purpose"]
                                                addResult(idInteraction)
                                                idInteraction += 1
                                                alertTime = 0
                                                acwTime = 0
                                                talkTime = 0
                                                holdTime = 0
                                                waitTime = 0
                                                operationTime = 0
                                                indOperationTime10 = 0
                                                indOperationTime15 = 0
                                                indOperationTime20 = 0
                                                indOperationTime30 = 0
                                                abandonTime = 0
                                                idAgent = ''
                                                idWrapup1 = ''
                                                noteWrapup1 = ''
                                                idWrapup2 = ''
                                                noteWrapup2 = ''
                                                idWrapup3 = ''
                                                noteWrapup3 = ''
                                                indDisconnect = 0
                                                disconnectType = 0
                                    elif nameIvrX == "FLUJO DE ENRUTAMIENTO DE TRANSFERENCIAS":
                                        acdDialTransfer = 'verifica componente transfer'
                                        if "transferTargetName" in f:
                                            acdDialTransfer = f["transferTargetName"]
                                        elif "transferTargetAddress" in f:
                                            acdDialTransfer = f["transferTargetAddress"]
                                        if indConsultX:
                                            indConsult = 1
                                            indConsultX = 0
                                            acdConsult = acdDialTransfer
                                            if indConsultTransferredX:
                                                indConsultTransferred = 1
                                        else:
                                            indTransfer = 1
                                            acdTransfer = acdDialTransfer
                                        if participantX:
                                            participant = participantX
                                            participantX = ''
                                        else:
                                            participant = p["purpose"]
                                        addResult(idInteraction)
                                        idInteraction += 1
                                        alertTime = 0
                                        acwTime = 0
                                        talkTime = 0
                                        holdTime = 0
                                        waitTime = 0
                                        operationTime = 0
                                        indOperationTime10 = 0
                                        indOperationTime15 = 0
                                        indOperationTime20 = 0
                                        indOperationTime30 = 0
                                        abandonTime = 0
                                        idAgent = ''
                                        idWrapup1 = ''
                                        noteWrapup1 = ''
                                        idWrapup2 = ''
                                        noteWrapup2 = ''
                                        idWrapup3 = ''
                                        noteWrapup3 = ''
                                        indDisconnect = 0
                                        disconnectType = 0
                                        # ACD
                        elif p["purpose"] == "acd":
                            notAcd = 0
                            if indCallPurecloud:
                                indCallPurecloud = 0
                            participant = p["purpose"]
                            if "participantName" in p:
                                nameAcd = p["participantName"]
                            for s in p["sessions"]:
                                mediaTypeAcd = s["mediaType"]
                                i = 0
                                for x in s["segments"]:
                                    if i == 0:
                                        segmentStartAcdFirst = x["segmentStart"]
                                    if "segmentEnd" in x:
                                        segmentEndAcdLast = x["segmentEnd"]
                                    else:
                                        segmentEndAcdLast = ""
                                    if "disconnectType" in x:
                                        if x["disconnectType"] != "transfer":
                                            indDisconnect = 1
                                            disconnectType = x["disconnectType"]

                                    if mediaTypeAcd == "callback":
                                        nameAcd = x["queueId"]
                                    i += 1
                                calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                    segmentStartAcdFirst, segmentEndAcdLast, config)
                                segmentStartAcd = calculateDates_segmentStart
                                segmentEndAcd = calculateDates_segmentEnd
                                segmentTimeAcd = calculateDates_segmentTime
                                if "metrics" in s:
                                    for m in s["metrics"]:
                                        if m["name"] == "tAbandon":
                                            indAbandonAcd = 1
                                            abandonTime = segmentTimeAcd
                                            waitTime = segmentTimeAcd
                                            if abandonTime <= 30:
                                                indAbandonTime30 = 1
                                                if abandonTime <= 20:
                                                    indAbandonTime20 = 1
                                                    if abandonTime <= 15:
                                                        indAbandonTime15 = 1
                                                        if abandonTime <= 10:
                                                            indAbandonTime10 = 1
                                            addResult(idInteraction)
                                            alertTime = 0
                                            acwTime = 0
                                            talkTime = 0
                                            holdTime = 0
                                            waitTime = 0
                                            operationTime = 0
                                            indOperationTime10 = 0
                                            indOperationTime15 = 0
                                            indOperationTime20 = 0
                                            indOperationTime30 = 0
                                            abandonTime = 0
                                            idAgent = ''
                                            idWrapup1 = ''
                                            noteWrapup1 = ''
                                            idWrapup2 = ''
                                            noteWrapup2 = ''
                                            idWrapup3 = ''
                                            noteWrapup3 = ''
                                            indDisconnect = 0
                                            disconnectType = 0
                        # AGENT
                        elif p["purpose"] == "agent":
                            participantX = ''
                            indConsultX = 0
                            indConsultTransferredX = 0
                            idAgent = p["userId"]
                            participant = p["purpose"]
                            if "participantName" in p:
                                if p["participantName"][0:3] == "Out":
                                    remoteExternal = p["participantName"]
                            operationTime = 0
                            waitTime = 0
                            disconnectType = 0
                            indDisconnect = 0
                            indOperationTime10 = 0
                            indOperationTime15 = 0
                            indOperationTime20 = 0
                            indOperationTime30 = 0
                            alertTime = 0
                            acwTime = 0
                            talkTime = 0
                            holdTime = 0
                            waitTime = 0
                            for s in p["sessions"]:
                                if "sessionDnis" in s:
                                    sessionDnis = s["sessionDnis"]

                                for x in s["segments"]:
                                    if notAcd:
                                        if "queueId" in x:
                                            nameAcd = x["queueId"]

                                    if x["segmentType"] == "alert":
                                        segmentStartAlertFirst = x["segmentStart"]
                                        if "segmentEnd" in x:
                                            segmentEndAlertLast = x["segmentEnd"]
                                        else:
                                            segmentEndAlertLast = ""
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartAlertFirst, segmentEndAlertLast, config)
                                        alertTime += calculateDates_segmentTime
                                    elif x["segmentType"] == "interact":
                                        segmentStartInteractFirst = x["segmentStart"]
                                        if "segmentEnd" in x:
                                            segmentEndInteractLast = x["segmentEnd"]
                                        else:
                                            segmentEndInteractLast = ''
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartInteractFirst, segmentEndInteractLast, config)
                                        talkTime += calculateDates_segmentTime
                                        if "disconnectType" in x:
                                            if x["disconnectType"] != "transfer":
                                                indDisconnect = 1
                                                disconnectType = x["disconnectType"]
                                    elif x["segmentType"] == "wrapup":
                                        segmentStartWrapupFirst = x["segmentStart"]
                                        if "segmentEnd" in x:
                                            segmentEndWrapupLast = x["segmentEnd"]
                                        else:
                                            segmentEndWrapupLast = ""
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartWrapupFirst, segmentEndWrapupLast, config)
                                        acwTime += calculateDates_segmentTime
                                        if "wrapUpNote" in x:
                                            idWrapups = x["wrapUpNote"].replace('\u25cf', '-', 5).replace('\u202c', '').replace('\u2070', '').replace('\U0001f6b2', '').replace('\u2717', '')
                                            if idWrapups.find("$") > 0:
                                                idWrapupsSplit = idWrapups.split('$')
                                                i = 0
                                                for idWrapup in idWrapupsSplit:
                                                    if idWrapup.find("!") > 0:
                                                        if idWrapup:
                                                            if i == 0:
                                                                idWrapup1, noteWrapup1 = idWrapup.split('!');
                                                            elif i == 1:
                                                                idWrapup2, noteWrapup2 = idWrapup.split('!');
                                                            elif i == 2:
                                                                idWrapup3, noteWrapup3 = idWrapup.split('!');
                                                        i += 1
                                                    else:
                                                        if "wrapUpCode" in x:
                                                            idWrapup1 = x["wrapUpCode"]
                                                        else:
                                                            idWrapup1 = "ERROR SIN WRAPUP"
                                                        break
                                        else:
                                            if "wrapUpCode" in x:
                                                idWrapup1 = x["wrapUpCode"]
                                            else:
                                                idWrapup1 = "ERROR SIN WRAPUP"
                                    elif x["segmentType"] == "hold":
                                        segmentStartHoldFirst = x["segmentStart"]
                                        if "segmentEnd" in x:
                                            segmentEndHoldLast = x["segmentEnd"]
                                        else:
                                            segmentEndHoldLast = ""
                                        calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                            segmentStartHoldFirst, segmentEndHoldLast, config)
                                        holdTime += calculateDates_segmentTime
                                if "metrics" in s:
                                    for m in s["metrics"]:
                                        if m["name"] == "nConsult":
                                            indTransferConsult = 1
                                            valueTransferConsult = m["value"]
                                            indConsultX = 1
                                        elif m["name"] == "nConsultTransferred":
                                            indTransferConsultTransferred = 1
                                            valueTransferConsultTransferred = m["value"]
                                            indConsultTransferredX = 1

                            operationTime = talkTime + holdTime + acwTime
                            waitTime = segmentTimeAcd + alertTime

                            if waitTime <= 30:
                                indOperationTime30 = 1
                                if waitTime <= 20:
                                    indOperationTime20 = 1
                                    if waitTime <= 15:
                                        indOperationTime15 = 1
                                        if waitTime <= 10:
                                            indOperationTime10 = 1

                            if indDisconnect:
                                # Solo se terminan llamadas con desconexión cuando no existe transferencia con
                                if indConsultX:
                                    participantX = p["purpose"]
                                else:
                                    participant = p["purpose"]
                                    addResult(idInteraction)
                                    idInteraction += 1
                                    alertTime = 0
                                    acwTime = 0
                                    talkTime = 0
                                    holdTime = 0
                                    waitTime = 0
                                    operationTime = 0
                                    indOperationTime10 = 0
                                    indOperationTime15 = 0
                                    indOperationTime20 = 0
                                    indOperationTime30 = 0
                                    abandonTime = 0
                                    idAgent = ''
                                    idWrapup1 = ''
                                    noteWrapup1 = ''
                                    idWrapup2 = ''
                                    noteWrapup2 = ''
                                    idWrapup3 = ''
                                    noteWrapup3 = ''
                                    indDisconnect = 0
                                    disconnectType = 0
                            else:
                                participantX = p["purpose"]

                            if mediaTypeAcd == "chat":
                                participant = p["purpose"]
                                addResult(idInteraction)
                                idInteraction += 1
                                alertTime = 0
                                acwTime = 0
                                talkTime = 0
                                holdTime = 0
                                waitTime = 0
                                operationTime = 0
                                indOperationTime10 = 0
                                indOperationTime15 = 0
                                indOperationTime20 = 0
                                indOperationTime30 = 0
                                abandonTime = 0
                                idAgent = ''
                                idWrapup1 = ''
                                noteWrapup1 = ''
                                idWrapup2 = ''
                                noteWrapup2 = ''
                                idWrapup3 = ''
                                noteWrapup3 = ''
                                indDisconnect = 0
                                disconnectType = 0

                        # EXTERNAL
                        elif p["purpose"] == "external":
                            participantX = ''
                            indConsultX = 0
                            indConsultTransferredX = 0
                            operationTime = 0
                            waitTime = 0
                            disconnectType = 0
                            indDisconnect = 0
                            indOperationTime10 = 0
                            indOperationTime15 = 0
                            indOperationTime20 = 0
                            indOperationTime30 = 0
                            alertTime = 0
                            acwTime = 0
                            talkTime = 0
                            holdTime = 0
                            waitTime = 0
                            participant = p["purpose"]
                            if "participantName" in p:
                                if p["participantName"][0:3] == "Out" or indCallPurecloud == 1:
                                    remoteExternal = p["participantName"]
                                    for s in p["sessions"]:
                                        disconnectType = 0
                                        indDisconnect = 0
                                        if "sessionDnis" in s:
                                            sessionDnis = s["sessionDnis"]
                                        for x in s["segments"]:
                                            if x["segmentType"] == "alert":
                                                segmentStartAlertFirst = x["segmentStart"]
                                                if "segmentEnd" in x:
                                                    segmentEndAlertLast = x["segmentEnd"]
                                                else:
                                                    segmentEndAlertLast = ""
                                                calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                                    segmentStartAlertFirst, segmentEndAlertLast, config)
                                                alertTime += calculateDates_segmentTime
                                            elif x["segmentType"] == "interact":
                                                segmentStartInteractFirst = x["segmentStart"]
                                                if "segmentEnd" in x:
                                                    segmentEndInteractLast = x["segmentEnd"]
                                                else:
                                                    segmentEndInteractLast = ''
                                                calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                                    segmentStartInteractFirst, segmentEndInteractLast, config)
                                                talkTime += calculateDates_segmentTime
                                                if "disconnectType" in x:
                                                    if x["disconnectType"] != "transfer":
                                                        indDisconnect = 1
                                                        disconnectType = x["disconnectType"]
                                            elif x["segmentType"] == "wrapup":
                                                segmentStartWrapupFirst = x["segmentStart"]
                                                if "segmentEnd" in x:
                                                    segmentEndWrapupLast = x["segmentEnd"]
                                                else:
                                                    segmentEndWrapupLast = ""
                                                calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                                    segmentStartWrapupFirst, segmentEndWrapupLast, config)
                                                acwTime += calculateDates_segmentTime
                                                if "wrapUpNote" in x:
                                                    idWrapups = x["wrapUpNote"].replace('\u25cf', '-', 5).replace(
                                                        '\u202c', '').replace('\u2070', '').replace('\U0001f6b2', '')
                                                    if idWrapups.find("$") > 0:
                                                        idWrapupsSplit = idWrapups.split('$')
                                                        i = 0
                                                        for idWrapup in idWrapupsSplit:
                                                            if idWrapup.find("!") > 0:
                                                                if idWrapup:
                                                                    if i == 0:
                                                                        idWrapup1, noteWrapup1 = idWrapup.split(
                                                                            '!');
                                                                    elif i == 1:
                                                                        idWrapup2, noteWrapup2 = idWrapup.split(
                                                                            '!');
                                                                    elif i == 2:
                                                                        idWrapup3, noteWrapup3 = idWrapup.split(
                                                                            '!');
                                                                i += 1
                                                            else:
                                                                if "wrapUpCode" in x:
                                                                    idWrapup1 = x["wrapUpCode"]
                                                                else:
                                                                    idWrapup1 = "ERROR SIN WRAPUP"
                                                                break
                                                else:
                                                    if "wrapUpCode" in x:
                                                        idWrapup1 = x["wrapUpCode"]
                                                    else:
                                                        idWrapup1 = "ERROR SIN WRAPUP"
                                            elif x["segmentType"] == "hold":
                                                # if iHold == 0:
                                                segmentStartHoldFirst = x["segmentStart"]
                                                if "segmentEnd" in x:
                                                    segmentEndHoldLast = x["segmentEnd"]
                                                else:
                                                    segmentEndHoldLast = ""
                                                calculateDates_segmentStart, calculateDates_segmentEnd, calculateDates_segmentTime = calculateDates(
                                                    segmentStartHoldFirst, segmentEndHoldLast, config)
                                                holdTime += calculateDates_segmentTime
                                        if "metrics" in s:
                                            for m in s["metrics"]:
                                                if m["name"] == "nConsult":
                                                    indTransferConsult = 1
                                                    valueTransferConsult = m["value"]
                                                    indConsultX = 1
                                                elif m["name"] == "nConsultTransferred":
                                                    indTransferConsultTransferred = 1
                                                    valueTransferConsultTransferred = m["value"]
                                                    indConsultTransferredX = 1
                                    operationTime = talkTime + holdTime + acwTime
                                    waitTime = segmentTimeAcd + alertTime

                                    if waitTime <= 30:
                                        indOperationTime30 = 1
                                        if waitTime <= 20:
                                            indOperationTime20 = 1
                                            if waitTime <= 15:
                                                indOperationTime15 = 1
                                                if waitTime <= 10:
                                                    indOperationTime10 = 1

                                    if indDisconnect:
                                        # Solo se terminan llamadas con desconexión cuando no existe transferencia con
                                        if indConsultX:
                                            participantX = p["purpose"]
                                        else:
                                            participant = p["purpose"]
                                            addResult(idInteraction)
                                            idInteraction += 1
                                            alertTime = 0
                                            acwTime = 0
                                            talkTime = 0
                                            holdTime = 0
                                            waitTime = 0
                                            operationTime = 0
                                            indOperationTime10 = 0
                                            indOperationTime15 = 0
                                            indOperationTime20 = 0
                                            indOperationTime30 = 0
                                            abandonTime = 0
                                            idAgent = ''
                                            idWrapup1 = ''
                                            noteWrapup1 = ''
                                            idWrapup2 = ''
                                            noteWrapup2 = ''
                                            idWrapup3 = ''
                                            noteWrapup3 = ''
                                            indDisconnect = 0
                                            disconnectType = 0
                                    else:
                                        participantX = p["purpose"]

                                else:
                                    for s in p["sessions"]:
                                        if "ani" in s:
                                            ani = s["ani"]
                                        if "dnis" in s:
                                            dnis = s["dnis"]

                config["payload"]["paging"]["pageNumber"] += 1
            else:
                break

        now = datetime.datetime.now()
        dt_string2 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha Fin conversationDetails: ", dt_string2)
        print("N. Conversations: ", numConversations)

        fecha1 = datetime.datetime.strptime(dt_string1, '%d/%m/%Y %H:%M:%S')
        fecha2 = datetime.datetime.strptime(dt_string2, '%d/%m/%Y %H:%M:%S')
        dias = (fecha2 - fecha1)
        print("Tiempo de proceso: ", round(dias.seconds / 60, 2), "minutos")
        return results
