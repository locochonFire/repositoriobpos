import pandas as pd
import dateutil
import datetime
import time
import chardet

class QueueReport:

    def __init__(self, urlCsv,fileName21,fileName22):
        self.urlCsv = urlCsv
        self.fileName21 = fileName21
        self.fileName22 = fileName22

    def findEncoding(self, fname):
        rfile = open(fname, 'rb').read()
        result = chardet.detect(rfile)
        charenc = result['encoding']
        return charenc


    def generateReport(self):
        now = datetime.datetime.now()
        dt_string1 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha inicio Queues =", dt_string1)	
     
        # Load data from csv file
        # data = pd.DataFrame.from_csv('conversationsDetails2020-01-07_00_23.csv', sep=';')
        # data = pd.read_csv('conversationsDetails2020-01-07_00_23.csv', sep=';')
        myEncoding = self.findEncoding(self.urlCsv)
        data = pd.read_csv(self.urlCsv, sep=';', low_memory=False, encoding = 'ISO-8859-1')
        # Convert date from string to date times
        data['conversationStart'] = data['conversationStart'].apply(
            dateutil.parser.parse, dayfirst=True)
        # data = read_csv(csv_path, sep=';')
        # print(data)
        #dataAgent = pd.read_csv(self.urlCsv, sep=';', low_memory=False)
        #dataAgent['conversationStart'] = dataAgent['conversationStart'].apply(
        #    dateutil.parser.parse, dayfirst=True)
        
        # Agregando columnas para anio, mes y dia
        #for i in range(1, len(data)):
        #    data.loc[i, 'Anio'] = data.loc[i, 'conversationStart'].year
        #    data.loc[i, 'Mes'] = data.loc[i, 'conversationStart'].month
        #    data.loc[i, 'Dia'] = data.loc[i, 'conversationStart'].day

        # print(data)
        data["acdTransfer"].fillna(" ", inplace = True) 
        data["nameAcd"].fillna(" ", inplace = True)
        data["mediaTypeAcd"].fillna(" ", inplace = True)
        data["idAgent"].fillna(" ", inplace = True)

        #SOlo de un agente
#        agent = data[(data.idAgent == 'ffd2802e-2216-4ac6-acf2-a75a73108374') & (data.nameAcd == 'RECLAMOS TARJETAS-PE')]
        
        #agent = data[(data.idAgent == 'ff33c2b3-4e0e-4790-bb44-13042a27c3a0')]
        #data.filter(like='46fd5813-4e59-4a03-b584-45e43b74153b', axis=0)
        #agent.to_csv('agent.csv', sep=';', encoding='utf-8')

        #conversacion = data[(data.idConversation == '44a61fd4-4f3b-4617-9f7c-4b4915039576')]
        #data.filter(like='46fd5813-4e59-4a03-b584-45e43b74153b', axis=0)
        #conversacion.to_csv('conversacion.csv', sep=';', encoding='utf-8')


        resultQueues = data.groupby(
            [
                #        data['conversationStart'].dt.year,
                #        data['conversationStart'].dt.month,
                #        data['conversationStart'].dt.day,
                'yearConversationStart',
                'monthConversationStart',
                'dayConversationStart',
                'intervalTime',
                'nameAcd',
                'originatingDirection',
                'mediaTypeAcd'
            ]
        ).agg(
            aceptadas=('accepted', sum),
            atendidas=('attended', sum),
            indOperationTime10=('indOperationTime10', sum),
            indOperationTime15=('indOperationTime15', sum),
            indOperationTime20=('indOperationTime20', sum),
            indOperationTime30=('indOperationTime30', sum),
            cantAbandonadas=('indAbandonAcd', sum),
            abandonTime=('abandonTime', sum),
            indAbandonTime10=('indAbandonTime10', sum),
            indAbandonTime15=('indAbandonTime15', sum),
            indAbandonTime20=('indAbandonTime20', sum),
            indAbandonTime30=('indAbandonTime30', sum),
            operationTimeMedia=('operationTime', 'mean'),
            talkTime=('talkTime', sum),
            acw=('acwTime', sum),
            holdTime=('holdTime', sum),
            segmentTimeAcd=('segmentTimeAcd', sum),
            waitTime=('waitTime', sum),
            waitTimeMedia=('waitTime', 'mean'),
            corte=('indDisconnect', sum),
            tipoCorte=('disconnectType', sum)
        )

        # result.to_csv(index=False)
        # Reporte de Colas
        resultQueues.to_csv(self.fileName21, sep=';', encoding='utf-8')

        # Reporte de Agentes
        resultAgents = data[data['participant'] == 'agent'].groupby(
            [
                'yearConversationStart',
                'monthConversationStart',
                'dayConversationStart',
                'idAgent',
                'nameAcd',
                'acdTransfer',
                'originatingDirection',
                'mediaTypeAcd'
            ]
        ).agg(
            atendidas=('attended', sum),
            operationTimeMedia=('operationTime', 'mean'),
            operationTimeSum=('operationTime', sum),
            talkTime=('talkTime', sum),
            acw=('acwTime', sum),
            holdTime=('holdTime', sum)
        ).sort_values(by='idAgent', ascending=False, na_position='first')

        resultAgents.to_csv(self.fileName22, sep=';', encoding='utf-8')

        now = datetime.datetime.now()
        dt_string2 = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Fecha Fin Queues =", dt_string2)
     
        fecha1 = datetime.datetime.strptime(dt_string1, '%d/%m/%Y %H:%M:%S')
        fecha2 = datetime.datetime.strptime(dt_string2, '%d/%m/%Y %H:%M:%S')
        dias = (fecha2 - fecha1) 
        print("Tiempo de proceso: ",round(dias.seconds/60,2), "minutos") 
        # print(result)
        # print(pd.__version__)
